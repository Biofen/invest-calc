import './App.css';
import React from 'react';
import Plot from 'react-plotly.js';
import Pot from './components/pot-component'


class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      potList: [],
      data: []
    }
    this.addNewPot = this.addNewPot.bind(this)
    this.handleDataUpdate = this.handleDataUpdate.bind(this)
  }

  addNewPot() {
    this.setState(prevState => ({
      potList: [...prevState.potList, new Pot()]
    }))
  }

  handleDataUpdate(potUpdate) {
    const newPotList = this.state.potList.slice()
    newPotList[potUpdate._reactInternals.key] = potUpdate
    this.setState({potList: newPotList}, () => {
      let newData = {
        x: potUpdate.x,
        y: potUpdate.y,
        type: potUpdate.type,
        mode: potUpdate.mode,
        marker: potUpdate.marker
      }
      const newDataList = this.state.data.slice()
      newDataList[0] = newData
      this.setState({data: newDataList})
    }) 
  }

  render() {
    return (
      <div addNewPlan="App">
        <header className="App-header">
          <p>Create New Pot</p>
          <button onClick={this.addNewPot}>Create</button>

          {this.state.potList.map((pot, index) => (
            <Pot key={index }  onDataUpdate={this.handleDataUpdate} ></Pot>
          ))}

          <Plot
            data={this.state.data}
            layout={ {title: 'A Fancy Plot', autosize: true} }
            useResizeHandler={true}
          />
        </header>
      </div>
    )
  }
}

export default App;
