import React from 'react';
import { Plan } from './plan-component'

class Pot extends React.Component {
    constructor(props) {
        super(props)
        this.x = []
        this.y = []
        this.type = 'scatter'
        this.mode = 'lines+markers'
        this.marker = {color: '#008000'}
        
        this.state = {
            planList: []
        }

        this.calculateFutureValue = this.calculateFutureValue.bind(this)
        this.calculate = this.calculate.bind(this)
        this.addNewPlan = this.addNewPlan.bind(this)
        this.handlePlanChange = this.handlePlanChange.bind(this)
    }



  calculateAccruedPrincipal(principal, rate) {
    var periods = 12
    var accrued = principal * (1 + rate/periods) ** periods
    // var interest = accrued - principal
    return accrued
  }

  calculateFutureValue(pmt, rate) {
    //https://www.thecalculatorsite.com/articles/finance/compound-interest-formula.php
    var n = 12 // Accruals per period
    var t = 1 // number of periods 
    // var fv = pmt * ((((1 + r/n)**(n*t)) - 1) / (r/n)) // End of month
    var fv = pmt * ((((1 + rate/n)**(n*t)) - 1) / (rate/n)) * (1 + rate/n) // Beginning of month
    // var interest = fv - pmt * n 

    return fv
  }

    calculate() {
        const newX = []
        const newY = []
        let currentYear = 0
        let currentEndYear = 0
        var principal = 0
        var rate = 0.07
        for (var i = 0; i < this.state.planList.length; i++) {
            
            let currentPlan = this.state.planList[i]
            currentEndYear = parseInt(currentPlan.state.years) + currentYear
            for (var j = currentYear; j < currentEndYear; j++) {
                principal = this.calculateAccruedPrincipal(principal, rate) + this.calculateFutureValue(currentPlan.state.monthlyContribution, rate)

                newX.push(j)
                newY.push(principal)
                
                this.x = newX
                this.y = newY

                this.props.onDataUpdate(this)
            }
            currentYear = currentEndYear
        }
        // var principal = 0
        // var rate = 0.07
        // var pmt = 700
        // let newData = Object.assign({}, this.state.data)
        // newData[0].y = []
        // newData[0].x = []
        // for (var i = 1; i <= this.state.years; i++) {
        // principal = this.calculateAccruedPrincipal(principal, rate) + this.calculateFutureValue(pmt, rate)
        // let newData = Object.assign({}, this.state.data)
        // newData[0].y.push(principal)
        // newData[0].x.push(i)
        // console.log(newData)
        // this.setState(newData)
        // }
    }
  

    addNewPlan() {
        this.setState(prevState => ({
            planList: [...prevState.planList, new Plan()]
          }))
    }

    handlePlanChange(planUpdate) {
        const newPlanList = this.state.planList.slice()
        newPlanList[planUpdate._reactInternals.key] = planUpdate
        this.setState({planList: newPlanList}, () => {
            this.calculate()
        }) 
    }

    render() {
        return (
            <div className="Pot">
                <p>Add a Plan</p>
                <button onClick={this.addNewPlan}>Create</button>

                {this.state.planList.map((plan, index) =>
                    <Plan {...plan} onPlanChange={this.handlePlanChange} key={index}/>
                )}

            </div>
        )
    }
}

export default Pot;