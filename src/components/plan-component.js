import React from 'react';

export class Plan extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            monthlyContribution: '',
            years: '',
        }

        this.updateMonthlyContribution = this.updateMonthlyContribution.bind(this)
        this.updateYears = this.updateYears.bind(this)
        this.canDoUpdate = this.canDoUpdate.bind(this)
    }

    updateMonthlyContribution(event) {
        this.setState({monthlyContribution: event.target.value}, () => {
            this.canDoUpdate()
        })
    }

    updateYears(event) {
        this.setState({years: event.target.value}, () => {
            this.canDoUpdate()
        })
    }

    canDoUpdate() {
        if (this.state.monthlyContribution !== '' && this.state.years !== '') {
            this.props.onPlanChange(this)
        }
    }


    render() {
        return (
            <div addNewPlan="Plan">
                <p>Monthly Contribution In £</p>
                <input type="number" value={this.state.monthlyContribution} onChange={this.updateMonthlyContribution}></input>
                <p>How Many Years</p>
                <input type="number" value={this.state.years} onChange={this.updateYears}></input>
            </div>
        )
    }
}